const fs = require('fs');

const directPath = './data/';
const pathJson = directPath+'mahasiswa.json';

if (!fs.existsSync(directPath)) {
  fs.mkdirSync(directPath);
}

if (!fs.existsSync(pathJson)) {
  fs.writeFileSync(pathJson, '[]', 'utf-8');
}

// Load data json
const loadDataMahasiswa = () => {
  // Membaca file json
  const fileBuffer = fs.readFileSync(pathJson, 'utf-8');
  return dataMahasiswa = JSON.parse(fileBuffer);
}

// Menyimpan data
exports.saveData = (mahasiswa) => {
  loadDataMahasiswa();
  
  // cek data duplikat
  const duplicates = dataMahasiswa.find(item => item.nim === mahasiswa.nim);
  if (duplicates) {
    console.log(`Data sudah ada`);
    return false;
  }
  dataMahasiswa.push(mahasiswa);

  // Menuliskan ke mahasiswa.json
  fs.writeFile(pathJson, JSON.stringify(dataMahasiswa, null, 2), (err) => {
    if (err) throw err;
    console.log('\n=== Data Mahasiswa telah tersimpan ===');
    console.table(dataMahasiswa)
  } )
}