const { saveData } = require('./inputDataMahasiswa');
const readline = require('node:readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
const util = require('node:util');
const question = util.promisify(rl.question).bind(rl);

const main = async () => {
  try {
    const nama = await question('Nama : ');
    const nim = await question('NIM : ');
    const jenis_kelamin = await question('Jenis Kelamin : ');
    const asal = await question('Asal Kota : ');
    if (nama.trim().length > 0 || nim.trim().length > 0 || jenis_kelamin.trim().length > 0 || asal.trim().length > 0) {
      // mahasiswa.push()
      const mahasiswa = {
        nama: `${nama}`,
        nim: `${nim}`,
        jenis_kelamin: `${jenis_kelamin}`,
        asal: `${asal}`
      }
      saveData(mahasiswa)
    } else {
      throw new Error(`empty`);
    }
  } catch (err) {
    console.error('Rejected', err);
  } finally {
    rl.close();
  }
}
main();